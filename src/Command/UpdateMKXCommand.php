<?php

namespace App\Command;

use App\Entity\Fighter;
use App\Entity\Game;
use App\Entity\GameVideo;
use App\Repository\FighterRepository;
use App\Repository\GameRepository;
use App\Service\OneXBetService;
use Doctrine\ORM\EntityManagerInterface;
use Enqueue\Client\ProducerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:update:mkx',
    description: 'Description',
)]
class UpdateMKXCommand extends Command
{
    const TIME_LIMIT = 57;

    private OneXBetService $oneXBetService;
    private GameRepository $gameRepository;
    private FighterRepository $fighterRepository;
    private EntityManagerInterface $em;
    private ProducerInterface $producer;

    public function __construct(
        OneXBetService $oneXBetService,
        GameRepository $gameRepository,
        FighterRepository $fighterRepository,
        ProducerInterface $producer,
        EntityManagerInterface $em
    )
    {
        parent::__construct();
        $this->gameRepository = $gameRepository;
        $this->fighterRepository = $fighterRepository;
        $this->oneXBetService = $oneXBetService;
        $this->producer = $producer;
        $this->em = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $start = microtime(true);
        while (true) {
            $matches = $this->oneXBetService->getMatches();
            foreach ($matches as $key => $match) {
                $game = $this->gameRepository->findOneById($match['I']);
                if(!$game) {
                    $leftFighter = $this->fighterRepository->findOneById($match['O1I']);
                    if(!$leftFighter) {
                        $leftFighter = new Fighter();
                        $leftFighter->setId($match['O1I']);
                        $leftFighter->setTitle($match['O1E']);
                        $leftFighter->setTitleRus($match['O1']);
                        $this->em->persist($leftFighter);
                    }
                    $rightFighter = $this->fighterRepository->findOneById($match['O2I']);
                    if(!$rightFighter) {
                        $rightFighter = new Fighter();
                        $rightFighter->setId($match['O2I']);
                        $rightFighter->setTitle($match['O2E']);
                        $rightFighter->setTitleRus($match['O2']);
                        $this->em->persist($rightFighter);
                    }
                    $game = new Game();
                    $game->setLeftFighter($leftFighter);
                    $game->setRightFighter($rightFighter);
                    $game->setId($match['I']);
                    $game->setStatus($this->em->getRepository(\App\Entity\GameStatus::class)->findOneById(1));
                    $game->setStartedAt((new \DateTime())->setTimestamp($match['S']));
                    $this->em->persist($game);
                    $video = new GameVideo();
                    $video->setGame($game);
                    $video->setSsid($match['VI'] ?? null);
                    $this->em->persist($video);
                    $this->em->flush();
                    $this->producer->sendCommand('matches', [
                        'id' => $game->getId(),
                        'error' => 0
                    ]);
                }
            }

            $current = microtime(true) - $start;
            if($current >= self::TIME_LIMIT) {
                break;
            }
            usleep(500000); // 0.5 sec
        }

        return Command::SUCCESS;
    }
}
