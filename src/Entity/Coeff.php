<?php

namespace App\Entity;

use App\Repository\CoeffRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoeffRepository::class)
 */
class Coeff
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $coeff;

    /**
     * @ORM\ManyToOne(targetEntity=CoeffTitle::class, inversedBy="coeffs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=CoeffGroup::class, inversedBy="coeffs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $section;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $time;

    /**
     * @ORM\ManyToOne(targetEntity=Round::class, inversedBy="coeffs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $round;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCoeff(): ?float
    {
        return $this->coeff;
    }

    public function setCoeff(float $coeff): self
    {
        $this->coeff = $coeff;

        return $this;
    }

    public function getTitle(): ?CoeffTitle
    {
        return $this->title;
    }

    public function setTitle(?CoeffTitle $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSection(): ?CoeffGroup
    {
        return $this->section;
    }

    public function setSection(?CoeffGroup $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(?float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getTime(): ?float
    {
        return $this->time;
    }

    public function setTime(?float $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getRound(): ?Round
    {
        return $this->round;
    }

    public function setRound(?Round $round): self
    {
        $this->round = $round;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
