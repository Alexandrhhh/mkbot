<?php

namespace App\Entity;

use App\Repository\CoeffTitleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoeffTitleRepository::class)
 */
class CoeffTitle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=Coeff::class, mappedBy="title", orphanRemoval=true)
     */
    private $coeffs;

    public function __construct()
    {
        $this->coeffs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Coeff[]
     */
    public function getCoeffs(): Collection
    {
        return $this->coeffs;
    }

    public function addCoeff(Coeff $coeff): self
    {
        if (!$this->coeffs->contains($coeff)) {
            $this->coeffs[] = $coeff;
            $coeff->setTitle($this);
        }

        return $this;
    }

    public function removeCoeff(Coeff $coeff): self
    {
        if ($this->coeffs->removeElement($coeff)) {
            // set the owning side to null (unless already changed)
            if ($coeff->getTitle() === $this) {
                $coeff->setTitle(null);
            }
        }

        return $this;
    }
}
