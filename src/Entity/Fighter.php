<?php

namespace App\Entity;

use App\Repository\FighterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FighterRepository::class)
 */
class Fighter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleRus;

    /**
     * @ORM\OneToMany(targetEntity=Round::class, mappedBy="winner")
     */
    private $winRounds;

    public function __construct()
    {
        $this->winRounds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitleRus(): ?string
    {
        return $this->titleRus;
    }

    public function setTitleRus(?string $titleRus): self
    {
        $this->titleRus = $titleRus;

        return $this;
    }

    /**
     * @return Collection|Round[]
     */
    public function getWinRounds(): Collection
    {
        return $this->winRounds;
    }

    public function addWinRound(Round $winRound): self
    {
        if (!$this->winRounds->contains($winRound)) {
            $this->winRounds[] = $winRound;
            $winRound->setWinner($this);
        }

        return $this;
    }

    public function removeWinRound(Round $winRound): self
    {
        if ($this->winRounds->removeElement($winRound)) {
            // set the owning side to null (unless already changed)
            if ($winRound->getWinner() === $this) {
                $winRound->setWinner(null);
            }
        }

        return $this;
    }
}
