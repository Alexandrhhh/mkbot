<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Fighter::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $leftFighter;

    /**
     * @ORM\ManyToOne(targetEntity=Fighter::class)
     */
    private $rightFighter;

    /**
     * @ORM\Column(type="integer")
     */
    private $leftScore = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $rightScore = 0;

    /**
     * @ORM\OneToMany(targetEntity=Round::class, mappedBy="match", orphanRemoval=true)
     */
    private $rounds;

    /**
     * @ORM\OneToOne(targetEntity=GameVideo::class, mappedBy="game", cascade={"persist", "remove"})
     */
    private $video;

    /**
     * @ORM\OneToMany(targetEntity=TGMessage::class, mappedBy="game")
     */
    private $tGMessages;

    /**
     * @ORM\ManyToOne(targetEntity=GameStatus::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startedAt;

    public function __construct()
    {
        $this->rounds = new ArrayCollection();
        $this->tGMessages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getLeftFighter(): ?Fighter
    {
        return $this->leftFighter;
    }

    public function setLeftFighter(?Fighter $leftFighter): self
    {
        $this->leftFighter = $leftFighter;

        return $this;
    }

    public function getRightFighter(): ?Fighter
    {
        return $this->rightFighter;
    }

    public function setRightFighter(?Fighter $rightFighter): self
    {
        $this->rightFighter = $rightFighter;

        return $this;
    }

    public function getLeftScore(): ?int
    {
        return $this->leftScore;
    }

    public function setLeftScore(int $leftScore): self
    {
        $this->leftScore = $leftScore;

        return $this;
    }

    public function getRightScore(): ?int
    {
        return $this->rightScore;
    }

    public function setRightScore(int $rightScore): self
    {
        $this->rightScore = $rightScore;

        return $this;
    }

    /**
     * @return Collection|Round[]
     */
    public function getRounds(): Collection
    {
        return $this->rounds;
    }

    public function addRound(Round $round): self
    {
        if (!$this->rounds->contains($round)) {
            $this->rounds[] = $round;
            $round->setMatch($this);
        }

        return $this;
    }

    public function removeRound(Round $round): self
    {
        if ($this->rounds->removeElement($round)) {
            // set the owning side to null (unless already changed)
            if ($round->getMatch() === $this) {
                $round->setMatch(null);
            }
        }

        return $this;
    }

    public function getVideo(): ?GameVideo
    {
        return $this->video;
    }

    public function setVideo(?GameVideo $video): self
    {
        // unset the owning side of the relation if necessary
        if ($video === null && $this->video !== null) {
            $this->video->setGame(null);
        }

        // set the owning side of the relation if necessary
        if ($video !== null && $video->getGame() !== $this) {
            $video->setGame($this);
        }

        $this->video = $video;

        return $this;
    }

    /**
     * @return Collection|TGMessage[]
     */
    public function getTGMessages(): Collection
    {
        return $this->tGMessages;
    }

    public function addTGMessage(TGMessage $tGMessage): self
    {
        if (!$this->tGMessages->contains($tGMessage)) {
            $this->tGMessages[] = $tGMessage;
            $tGMessage->setGame($this);
        }

        return $this;
    }

    public function removeTGMessage(TGMessage $tGMessage): self
    {
        if ($this->tGMessages->removeElement($tGMessage)) {
            // set the owning side to null (unless already changed)
            if ($tGMessage->getGame() === $this) {
                $tGMessage->setGame(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?GameStatus
    {
        return $this->status;
    }

    public function setStatus(?GameStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(\DateTimeInterface $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }
}
