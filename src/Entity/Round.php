<?php

namespace App\Entity;

use App\Repository\RoundRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoundRepository::class)
 */
class Round
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="rounds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity=Fighter::class, inversedBy="winRounds")
     */
    private $winner;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $time;

    /**
     * @ORM\Column(type="integer")
     */
    private $leftScore = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $rightScore = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Finish::class, inversedBy="rounds")
     */
    private $finish;

    /**
     * @ORM\OneToMany(targetEntity=Coeff::class, mappedBy="round", orphanRemoval=true)
     */
    private $coeffs;

    public function __construct()
    {
        $this->coeffs = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getWinner(): ?Fighter
    {
        return $this->winner;
    }

    public function setWinner(?Fighter $winner): self
    {
        $this->winner = $winner;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(?int $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getLeftScore(): ?int
    {
        return $this->leftScore;
    }

    public function setLeftScore(int $leftScore): self
    {
        $this->leftScore = $leftScore;

        return $this;
    }

    public function getRightScore(): ?int
    {
        return $this->rightScore;
    }

    public function setRightScore(int $rightScore): self
    {
        $this->rightScore = $rightScore;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getFinish(): ?Finish
    {
        return $this->finish;
    }

    public function setFinish(?Finish $finish): self
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * @return Collection|Coeff[]
     */
    public function getCoeffs(): Collection
    {
        return $this->coeffs;
    }

    public function addCoeff(Coeff $coeff): self
    {
        if (!$this->coeffs->contains($coeff)) {
            $this->coeffs[] = $coeff;
            $coeff->setRound($this);
        }

        return $this;
    }

    public function removeCoeff(Coeff $coeff): self
    {
        if ($this->coeffs->removeElement($coeff)) {
            // set the owning side to null (unless already changed)
            if ($coeff->getRound() === $this) {
                $coeff->setRound(null);
            }
        }

        return $this;
    }
}
