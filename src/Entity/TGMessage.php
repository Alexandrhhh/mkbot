<?php

namespace App\Entity;

use App\Repository\TGMessageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TGMessageRepository::class)
 */
class TGMessage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint")
     */
    private $messageId;

    /**
     * @ORM\ManyToOne(targetEntity=TGChat::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $chat;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="tGMessages")
     */
    private $game;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     */
    private $reply;

    /**
     * @ORM\ManyToOne(targetEntity=TGMessage::class, inversedBy="replies")
     */
    private $messageReply;

    /**
     * @ORM\OneToMany(targetEntity=TGMessage::class, mappedBy="messageReply")
     */
    private $replies;

    /**
     * @ORM\Column(type="boolean")
     */
    private $forward;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $coeff;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $success;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $round;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Donation::class, inversedBy="tGMessages")
     */
    private $donation;

    /**
     * @ORM\ManyToOne(targetEntity=TGUser::class, inversedBy="messages")
     */
    private $usr;

    /**
     * @ORM\ManyToOne(targetEntity=TGUser::class, inversedBy="forwards")
     */
    private $forwardUser;

    public function __construct()
    {
        $this->replies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessageId(): ?string
    {
        return $this->messageId;
    }

    public function setMessageId(string $messageId): self
    {
        $this->messageId = $messageId;

        return $this;
    }

    public function getChat(): ?TGChat
    {
        return $this->chat;
    }

    public function setChat(?TGChat $chat): self
    {
        $this->chat = $chat;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getReply(): ?bool
    {
        return $this->reply;
    }

    public function setReply(bool $reply): self
    {
        $this->reply = $reply;

        return $this;
    }

    public function getMessageReply(): ?self
    {
        return $this->messageReply;
    }

    public function setMessageReply(?self $messageReply): self
    {
        $this->messageReply = $messageReply;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getReplies(): Collection
    {
        return $this->replies;
    }

    public function addReply(self $reply): self
    {
        if (!$this->replies->contains($reply)) {
            $this->replies[] = $reply;
            $reply->setMessageReply($this);
        }

        return $this;
    }

    public function removeReply(self $reply): self
    {
        if ($this->replies->removeElement($reply)) {
            // set the owning side to null (unless already changed)
            if ($reply->getMessageReply() === $this) {
                $reply->setMessageReply(null);
            }
        }

        return $this;
    }

    public function getForward(): ?bool
    {
        return $this->forward;
    }

    public function setForward(bool $forward): self
    {
        $this->forward = $forward;

        return $this;
    }

    public function getCoeff(): ?float
    {
        return $this->coeff;
    }

    public function setCoeff(?float $coeff): self
    {
        $this->coeff = $coeff;

        return $this;
    }

    public function getSuccess(): ?bool
    {
        return $this->success;
    }

    public function setSuccess(?bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    public function getRound(): ?int
    {
        return $this->round;
    }

    public function setRound(?int $round): self
    {
        $this->round = $round;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getDonation(): ?Donation
    {
        return $this->donation;
    }

    public function setDonation(?Donation $donation): self
    {
        $this->donation = $donation;

        return $this;
    }

    public function getUsr(): ?TGUser
    {
        return $this->usr;
    }

    public function setUsr(?TGUser $usr): self
    {
        $this->usr = $usr;

        return $this;
    }

    public function getForwardUser(): ?TGUser
    {
        return $this->forwardUser;
    }

    public function setForwardUser(?TGUser $forwardUser): self
    {
        $this->forwardUser = $forwardUser;

        return $this;
    }
}
