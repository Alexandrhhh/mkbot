<?php

namespace App\Entity;

use App\Repository\TGUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TGUserRepository::class)
 */
class TGUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBot;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $language_code;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=TGMessage::class, mappedBy="usr")
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity=TGMessage::class, mappedBy="forwardUser")
     */
    private $forwards;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->forwards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsBot(): ?bool
    {
        return $this->isBot;
    }

    public function setIsBot(bool $isBot): self
    {
        $this->isBot = $isBot;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLanguageCode(): ?string
    {
        return $this->language_code;
    }

    public function setLanguageCode(?string $language_code): self
    {
        $this->language_code = $language_code;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|TGMessage[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(TGMessage $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUsr($this);
        }

        return $this;
    }

    public function removeMessage(TGMessage $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getUsr() === $this) {
                $message->setUsr(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TGMessage[]
     */
    public function getForwards(): Collection
    {
        return $this->forwards;
    }

    public function addForward(TGMessage $forward): self
    {
        if (!$this->forwards->contains($forward)) {
            $this->forwards[] = $forward;
            $forward->setForwardUser($this);
        }

        return $this;
    }

    public function removeForward(TGMessage $forward): self
    {
        if ($this->forwards->removeElement($forward)) {
            // set the owning side to null (unless already changed)
            if ($forward->getForwardUser() === $this) {
                $forward->setForwardUser(null);
            }
        }

        return $this;
    }
}
