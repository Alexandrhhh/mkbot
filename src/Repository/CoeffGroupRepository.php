<?php

namespace App\Repository;

use App\Entity\CoeffGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CoeffGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoeffGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoeffGroup[]    findAll()
 * @method CoeffGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoeffGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CoeffGroup::class);
    }

    // /**
    //  * @return CoeffGroup[] Returns an array of CoeffGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CoeffGroup
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
