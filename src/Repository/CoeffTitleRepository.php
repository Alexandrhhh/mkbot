<?php

namespace App\Repository;

use App\Entity\CoeffTitle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CoeffTitle|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoeffTitle|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoeffTitle[]    findAll()
 * @method CoeffTitle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoeffTitleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CoeffTitle::class);
    }

    // /**
    //  * @return CoeffTitle[] Returns an array of CoeffTitle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CoeffTitle
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
