<?php

namespace App\Repository;

use App\Entity\GameVideo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GameVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameVideo[]    findAll()
 * @method GameVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameVideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GameVideo::class);
    }

    // /**
    //  * @return GameVideo[] Returns an array of GameVideo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameVideo
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
