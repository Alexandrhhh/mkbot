<?php

namespace App\Repository;

use App\Entity\TGChat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TGChat|null find($id, $lockMode = null, $lockVersion = null)
 * @method TGChat|null findOneBy(array $criteria, array $orderBy = null)
 * @method TGChat[]    findAll()
 * @method TGChat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TGChatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TGChat::class);
    }

    // /**
    //  * @return TGChat[] Returns an array of TGChat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TGChat
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
