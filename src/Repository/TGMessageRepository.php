<?php

namespace App\Repository;

use App\Entity\TGMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TGMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method TGMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method TGMessage[]    findAll()
 * @method TGMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TGMessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TGMessage::class);
    }

    // /**
    //  * @return TGMessage[] Returns an array of TGMessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TGMessage
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
