<?php

namespace App\Repository;

use App\Entity\TGUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TGUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method TGUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method TGUser[]    findAll()
 * @method TGUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TGUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TGUser::class);
    }

    // /**
    //  * @return TGUser[] Returns an array of TGUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TGUser
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
