<?php


namespace App\Service;


use App\Entity\Coeff;
use App\Entity\CoeffGroup;
use App\Entity\CoeffTitle;
use App\Entity\Finish;
use App\Entity\Game;
use App\Entity\GameStatus;
use App\Entity\Round;
use App\Repository\GameRepository;
use App\Repository\RoundRepository;
use App\Service\Processor\MatchesProcessor;
use Doctrine\ORM\EntityManagerInterface;
use Enqueue\Client\Message;
use Enqueue\Client\ProducerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class OneXBetService
{
    const BASE_URI = 'https://1xstavka.ru';
    const GET_MATCHES = '/LiveFeed/BestGamesExtZip';
    const GET_MATCH = '/LiveFeed/GetGameZip';
    const CHAMP = 1252965;
    const SPORT = 103;
    const COUNT = 10;
    const MODE = 4;
    const ID = 100802;
    const GAME = 'Mortal Kombat X';

    private Client $client;
    private GameRepository $gameRepository;
    private RoundRepository $roundRepository;
    private EntityManagerInterface $em;
    private ProducerInterface $producer;

    public function __construct(
        GameRepository $gameRepository,
        RoundRepository $roundRepository,
        EntityManagerInterface $em,
        ProducerInterface $producer
    )
    {
        $this->client = new Client([
            'base_uri' => self::BASE_URI,
        ]);
        $this->gameRepository = $gameRepository;
        $this->roundRepository = $roundRepository;
        $this->em = $em;
        $this->producer = $producer;
    }

    public function getMatches()
    {
        try {
            $body = $this->client->get(self::GET_MATCHES, [
                'query' => [
                    'champs' => self::CHAMP,
                    'sports' => self::SPORT,
                    'count' => self::COUNT,
                    'mode' => self::MODE,
                    'id' => self::ID,
                ]
            ])->getBody()->getContents();
        } catch (\Exception $exception) {
            sleep(1);
            return null;
        }

        $matches = json_decode($body, 1)['Value'] ?? null;

        foreach ($matches as $key => $match) {
            if($match['L'] != self::GAME && $match['LI'] != self::CHAMP) {
                unset($matches[$key]);
            }
        }

        return $matches;
    }

    public function updateGame(Game $game, $error = 0): string
    {
        if($game->getStatus()->getId() == 2) {
            try {
                $result = $this->client->get(self::GET_MATCH, [
                    'query' => [
                        'id' => $game->getId()
                    ]
                ]);
            } catch (GuzzleException $e) {
                if($error > 3) {
                    return MatchesProcessor::REJECT;
                }
                $this->producer->sendCommand('matches', [
                    'id' => $game->getId(),
                    'error' => $error + 1
                ]);
                return MatchesProcessor::REJECT;
            }
            $result = json_decode((string) $result->getBody(), 1)['Value'];
            $names = json_decode(file_get_contents('https://churin.pro/betname.json'), true);
            $groups = json_decode(file_get_contents('https://churin.pro/betname_group.json'), true);

            $number = $result['SC']['CP'] ?? 0; //номер раунда

            $current_round = $this->firstOrCreateRound($game, $number);

            if(!isset($result['SC']['S'][1]['Value'])) {
                return MatchesProcessor::REJECT;
            }

            $stores = json_decode($result['SC']['S'][1]['Value'], 1);

            $left = $game->getLeftFighter();
            $right = $game->getRightFighter();
            if(!empty($stores)) {
                foreach($stores as $store) {
                    $round = $this->firstOrCreateRound($game, $store['R']);
                    $finish = $this->firstOrCreateFinish($store['DI']);
                    if($store['R'] == 1) {
                        $leftScore = ($store['W'] == strtoupper($left->getTitle()) || $store['W'] == $left->getTitleRus()) ? 1 : 0;
                        $rightScore = ($store['W'] == strtoupper($right->getTitle()) || $store['W'] == $right->getTitleRus()) ? 1 : 0;
                        $round->setLeftScore($leftScore);
                        $round->setRightScore($rightScore);
                    } else {
                        $prev_round = $this->firstOrCreateRound($game, $store['R']-1);
                        $leftScore = ($store['W'] == strtoupper($left->getTitle()) || $store['W'] == $left->getTitleRus()) ? ($prev_round->getLeftScore() + 1) : $prev_round->getLeftScore();
                        $rightScore = ($store['W'] == strtoupper($right->getTitle()) || $store['W'] == $right->getTitleRus()) ? ($prev_round->getRightScore() + 1) : $prev_round->getRightScore();
                        $round->setLeftScore($leftScore);
                        $round->setRightScore($rightScore);
                    }
                    $winner = ($store['W'] == \strtoupper($left->getTitle()) || $store['W'] == $left->getTitleRus()) ? $left : $right;
                    $round->setWinner($winner);
                    $round->setFinish($finish);
                    $round->setTime($store['T']);
                }
            }

            foreach ($result['E'] as $key => $kf) {
                $time = null;
                $value = null;
                if(isset($kf['P']) && $kf['P'] > 10) {
                    $arr = explode('.', $kf['P']);
                    $value = $arr[0]/100;
                    $time = $arr[1]/10;
                }
                $name_id = $kf['T'];
                $group_id = (int)$names[$name_id]['IdG'];
                $coeffTitle = $this->firstOrCreateCoeffTitle($name_id, $names[$name_id]["N"]);
                $coeffGroup = $this->firstOrCreateCoeffGroup($group_id, $groups[$group_id]["N"]);
                $value = isset($kf['P']) ? (($value) ? $value : $kf['P']) : null;
                $this->firstOrCreateCoeff($coeffTitle, $coeffGroup, $current_round, $kf['C'], $value, $time);
            }

            $leftScore = $result['SC']['FS']['S1'] ?? 0;
            $rightScore = $result['SC']['FS']['S2'] ?? 0;
            $game->setLeftScore($leftScore);
            $game->setRightScore($rightScore);

            if(isset($result['SC']['CPS']) && $result['SC']['CPS'] == 'Игра завершена') {
                $game->setStatus($this->em->getRepository(GameStatus::class)->findOneBy(['id' => 3]));
            }
            
            $this->em->flush();
            $message = new Message([
                'id' => $game->getId(),
                'error' => $error
            ]);
            $message->setDelay(5);
            $this->producer->sendCommand('matches', $message);
            return MatchesProcessor::ACK;
        }
        return MatchesProcessor::REJECT;
    }

    private function firstOrCreateRound(Game $game, $number = 0): object
    {
        $round = $this->roundRepository->findOneBy([
            'game' => $game,
            'number' => $number
        ]);
        if(!$round) {
            $round = new Round();
            $round->setGame($game);
            $round->setNumber($number);
            $this->em->persist($round);
            $this->em->flush();
        }
        return $round;
    }

    private function firstOrCreateFinish($title = ''): object
    {
        $finish = $this->em->getRepository(Finish::class)->findOneBy(['title' => $title]);
        if(!$finish) {
            $finish = new Finish();
            $finish->setTitle($title);
            $this->em->persist($finish);
            $this->em->flush();
        }
        return $finish;
    }

    private function firstOrCreateCoeff(
        CoeffTitle $title,
        CoeffGroup $group,
        Round $round,
        float $kf,
        $value = null,
        $time = null
    ): object
    {
        $coeff = $this->em->getRepository(Coeff::class)->findOneBy([
            'title' => $title,
            'section' => $group,
            'round' => $round,
            'value' => $value,
            'time' => $time
        ]);
        if(!$coeff) {
            $coeff = new Coeff();
            $coeff->setTitle($title);
            $coeff->setSection($group);
            $coeff->setRound($round);
            $coeff->setValue($value);
            $coeff->setTime($time);
            $coeff->setCoeff($kf);
            $this->em->persist($coeff);
            $this->em->flush();
            return $coeff;
        }
        if($coeff->getCoeff() != $kf){
            $coeff->setCoeff($kf);
            $this->em->flush();
        }
        return $coeff;
    }

    private function firstOrCreateCoeffTitle($id, $title = ''): object
    {
        $coeffTitle = $this->em->getRepository(CoeffTitle::class)->findOneBy([
            'id' => $id
        ]);
        if(!$coeffTitle) {
            $coeffTitle = new CoeffTitle();
            $coeffTitle->setId($id);
            $coeffTitle->setTitle($title);
            $this->em->persist($coeffTitle);
            $this->em->flush();
        }
        return $coeffTitle;
    }

    private function firstOrCreateCoeffGroup($id, $title = ''): object
    {
        $coeffGroup = $this->em->getRepository(CoeffGroup::class)->findOneBy([
            'id' => $id
        ]);
        if(!$coeffGroup) {
            $coeffGroup = new CoeffGroup();
            $coeffGroup->setId($id);
            $coeffGroup->setTitle($title);
            $this->em->persist($coeffGroup);
            $this->em->flush();
        }
        return $coeffGroup;
    }
}