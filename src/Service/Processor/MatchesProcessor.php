<?php


namespace App\Service\Processor;

use App\Repository\GameRepository;
use App\Repository\GameStatusRepository;
use App\Service\OneXBetService;
use Doctrine\ORM\EntityManagerInterface;
use Enqueue\Client\CommandSubscriberInterface;
use Enqueue\Client\ProducerInterface;
use Interop\Queue\Message;
use Interop\Queue\Context;
use Interop\Queue\Processor;

class MatchesProcessor implements Processor, CommandSubscriberInterface
{
    private GameRepository $gameRepository;
    private ProducerInterface $producer;
    private EntityManagerInterface $em;
    private GameStatusRepository $statusRepository;
    private OneXBetService $oneXBetService;

    public function __construct(
        GameRepository $gameRepository,
        GameStatusRepository $gameStatusRepository,
        OneXBetService $oneXBetService,
        ProducerInterface $producer,
        EntityManagerInterface $em
    )
    {
        $this->gameRepository = $gameRepository;
        $this->statusRepository = $gameStatusRepository;
        $this->oneXBetService = $oneXBetService;
        $this->producer = $producer;
        $this->em = $em;
    }

    public function process(Message $message, Context $context): object|string
    {
        $body = json_decode($message->getBody(), 1);
        $game = $this->gameRepository->findOneById($body['id']);

        if(!$game) {
            return self::REJECT;
        }

        if($game->getStatus()->getId() == 1) {
            $game->setStatus($this->statusRepository->findOneById(2));
            $this->producer->sendCommand('matches', [
                'id' => $game->getId(),
                'error' => 0
            ]);
            $this->em->flush();
            $this->em->clear();
            $this->em->getConnection()->close();
            return self::ACK;
        }

        return $this->oneXBetService->updateGame($game, $body['error']);
    }

    /**
     * @return array
     */
    public static function getSubscribedCommand(): array
    {
        return [
            'processorName' => 'matches',
            'queueName' => 'matches',
            'queueNameHardcoded' => true,
            'exclusive' => true,
        ];
    }
}